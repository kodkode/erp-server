import { connect } from "mongoose";
import "dotenv/config";

const URL = process.env.MONGODB_URI;

const connectionToDb = async () => {
  
    let isConnect = false
    let retries = 5;
    while (!isConnect && retries > 0) {
      try {
        await connect(URL as string);
        isConnect = true
        return 'Connected to MongoDB'
      } catch (error) {
        console.error('Error connecting to MongoDB:', error)
        console.log(`Retries left: ${--retries}`)
        await new Promise((resolve) => setTimeout(resolve, 5000)) // Wait for 5 seconds before retrying
      }
    }
    if (!isConnect) {
      return 'Failed to connect to MongoDB after multiple attempts.'
    }
  }


export default connectionToDb;
