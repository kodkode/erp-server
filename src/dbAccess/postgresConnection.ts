import {Pool} from "pg";
import "dotenv/config";
import initialData from "../utils/initialData";

export const client = new Pool({
  connectionString: process.env.POSTGRESQL_CONNECTION_STRING,
});

export const connectionToPostgres = async () => {
  let isConnect = false;
  let retries = 5;
  while (!isConnect && retries > 0) {
    try {
      await client.connect();
      isConnect = true;
      console.log("Connected to Postgres");
      await initialData();
      return;
    } catch (error) {
      console.error("Error connecting to Postgres:", error);
      console.log(`Retries left: ${--retries}`);
      await new Promise((resolve) => setTimeout(resolve, 5000)); // Wait for 5 seconds before retrying
    }
  }
  if (!isConnect) {
    return console.log(
      "Failed to connect to Postgres after multiple attempts."
    );
  }
};
