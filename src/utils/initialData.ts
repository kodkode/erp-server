import chalk from "chalk";
import {client} from "../dbAccess/postgresConnection";
import {
  addDataToTableQuery,
  addTableIfNotExistQuery,
  checkExistDataQuery,
} from "./queries";

const initialData = async () => {
  try {
    console.log(chalk.cyan(`Initial table...`));
    await client.query(addTableIfNotExistQuery);
    const data = await client.query(checkExistDataQuery);
    if (data.rows.length !== 0)
      return console.log(chalk.blueBright("Data already exist "));
    console.log(chalk.greenBright(`Add data to table...`));
    await client.query(addDataToTableQuery);
    return console.log("Postgres Database initial successfully");
  } catch (error) {
    console.log(error);
    return console.log("Error has accord during initial data");
  }
};

export default initialData;
