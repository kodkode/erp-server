FROM node:lts-slim AS build

WORKDIR /app

COPY package*.json ./
COPY tsconfig.json ./
COPY jest.config.js ./
COPY __tests__ ./__tests__
RUN ls
RUN npm install


CMD ["npm", "run", "test:e2e"]
